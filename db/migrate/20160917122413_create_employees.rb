class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :firstname
      t.string :lastname
      t.string :pkz
      t.string :address
      t.string :position
      t.string :room_number
      t.integer :superior_id
      t.string :telephone
      t.string :email
      t.string :social_security_numer

      t.timestamps null: false
    end
  end
end
