class SplitAddress < ActiveRecord::Migration
  def change
    remove_column :employees, :address
    add_column :employees, :street, :string
    add_column :employees, :street_nr, :integer
    add_column :employees, :post_code, :integer
    add_column :employees, :city, :string
    add_column :employees, :country, :string
  end
end
