class AddSocialNetworkUserId < ActiveRecord::Migration
  def change
    add_column :employees, :social_network_user_id, :integer
  end
end
