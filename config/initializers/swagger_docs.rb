Swagger::Docs::Config.register_apis({
  "1.0" => {
    # the extension used for the API
    :api_extension_type => nil,
    # the output location where your .json files are written to
    :api_file_path => 'public/api/',
    # if you want to delete all .json files at each generation
    :clean_directory => false,
    # add custom attributes to api-docs
    :attributes => {
      :info => {
        "title" => "This is the employee management system"
      }
    }
  }
})