class EmployeesController < ApplicationController
  include Swagger::Docs::ImpotentMethods

  before_filter :authenticate_user!, except: [:index, :show]
  before_action :set_employee, only: [:show, :edit, :update, :destroy]

  swagger_controller :employees, "Employee Management"

  swagger_api :index do
    summary "Fetches all employees"
    notes "This lists all the active users"
  end

  swagger_api :show do
    consumes ["application/json"]
    summary "Fetches a single employee item"
    param :path, :id, :integer, :optional, "User Id"
  end

  swagger_api :create do
    summary "Creates a new employee"
    notes "This creates a new employee and saves it to the database"
    consumes ["application/json"]
    param :header, 'X-CSRF-Token', :string, :required, 'X-CSRF-Token'
    param :body, :body, :employee, :required, "Employee object"
    response :ok, "Employee was successfully created."
    response :unauthorized
    response :unprocessable_entity
  end

  swagger_api :update do
    summary "Updates an existing employee"
    notes "This updates an existing employee and saves the changes to the database"
    consumes ["application/json"]
    param :header, 'X-CSRF-Token', :string, :required, 'X-CSRF-Token'
    param :path, :id, :integer, :required, "User Id"
    param :body, :body, :employee, :required, "Employee object"
    response :ok, "Employee was successfully updated."
    response :unauthorized
    response :unprocessable_entity
  end

  swagger_api :destroy do
    summary "Deletes an existing employee"
    notes "This deletes an existing employee from the database"
    consumes ["application/json"]
    param :header, 'X-CSRF-Token', :string, :required, 'X-CSRF-Token'
    param :path, :id, :integer, :required, "User Id"
    response :ok, "Employee was successfully deleted."
    response :unauthorized
  end

  swagger_model :employee do
    description "employee object"
    property :id, :integer, :required, "User Id"
    property :user_name, :string, :optional, "User name"
    property :firstname, :string, :optional, "First name"
    property :lastname, :string, :optional, "Last name"
    property :pkz, :string, :optional, "PKZ"
    property :street, :string, :optional, "Street"
    property :street_nr, :integer, :optional, "Street Nr" 
    property :post_code, :integer, :optional, "Postcode" 
    property :city, :string, :optional, "City" 
    property :country, :string, :optional, "Country"  
    property :position, :string, :optional, "Position in Company"
    property :room_number, :string, :optional, "Room number" 
    property :superior_id, :string, :optional, "Superior ID" 
    property :telephone, :string, :optional, "Telephone number" 
    property :email, :string, :optional, "Email" 
    property :social_security_numer, :string, :optional, "Social security number"
    property :credit_card_nr, :string, :optional, "Credit card number"
    property :expiration_date, :string, :optional, "Credit card expiration date"
  end


  # GET /employees
  # GET /employees.json
  def index
    @employees = Employee.all
  end

  # GET /employees/1
  # GET /employees/1.json
  def show
    if @employee.user_name.blank?
      flash.now[:notice] = "Social Network Service is not available or the user is not registered on the Social Network Service"
    end
  end

  # GET /employees/new
  def new
    @employees = Employee.all
    @employee = Employee.new
  end

  # GET /employees/1/edit
  def edit
    @employees = Employee.all
  end

  # POST /employees
  # POST /employees.json
  def create
    @employee = Employee.new(employee_params)

    
    respond_to do |format|
      begin
        ActiveRecord::Base.transaction do
          @employee.save!
          uri = URI.parse("http://socialnetworkusers.azurewebsites.net/api/Users")
          http = Net::HTTP.new(uri.host, uri.port)
          request = Net::HTTP::Post.new(uri.path, 'Content-Type' => 'application/json')
          json = { :UserName => @employee.user_name,   
                  :FirstName => @employee.firstname, 
                  :LastName => @employee.lastname, 
                  :Email => @employee.email, 
                  :ExpirationDate => @employee.expiration_date.to_time.iso8601,
                  :Street => @employee.street,
                  :StreetNr => @employee.street_nr,
                  :PostCode => @employee.post_code,
                  :City => @employee.city,
                  :CreditCardNr => @employee.credit_card_nr,
                  :Country => @employee.country}.to_json
          request.body = json
          begin
            response = http.request(request)
          rescue
            format.html { redirect_to @employee, notice: 'Employee was successfully created but social network could not be reached.' }
            format.json { render :show, status: :ok, location: @employee }
            return
          end

          if response.code == '201'
            @employee.set_attributes_from_social_media_service JSON.parse(response.body)['user']
            @employee.save!
            format.html { redirect_to @employee, notice: 'Employee was successfully created.' }
          else
            format.html { redirect_to @employee, notice: 'Employee was successfully created but could not be created in the social network.' }
          end  
          format.json { render :show, status: :created, location: @employee }
        end
      rescue ActiveRecord::RecordInvalid => exception
        @employees = Employee.all
        format.html { render :new }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update

    respond_to do |format|
      if @employee.update(employee_params)
        # using other micro-service to update user data
        uri = URI.parse("http://socialnetworkusers.azurewebsites.net/api/Users/#{@employee.social_network_user_id}")
        http = Net::HTTP.new(uri.host, uri.port)
        request = Net::HTTP::Put.new(uri.path, 'Content-Type' => 'application/json')
        json = { :Id => @employee.social_network_user_id,
                 :UserName => @employee.user_name,
                 :FirstName => @employee.firstname, 
                 :LastName => @employee.lastname, 
                 :Email => @employee.email, 
                 :ExpirationDate => @employee.expiration_date.to_time.iso8601,
                 :Street => @employee.street,
                 :StreetNr => @employee.street_nr,
                 :PostCode => @employee.post_code,
                 :City => @employee.city,
                 :CreditCardNr => @employee.credit_card_nr,
                 :Country => @employee.country}.to_json
        request.body = json
        begin
          response = http.request(request)
        rescue
          format.html { redirect_to @employee, notice: 'Employee was successfully updated but social network could not be reached.' }
          format.json { render :show, status: :ok, location: @employee }
          return
        end

        if response.code == '204'
          format.html { redirect_to @employee, notice: 'Employee was successfully updated.' }
        else 
          format.html { redirect_to @employee, notice: 'Employee was successfully updated but could not be updated in the social network.' }
        end
        format.json { render :show, status: :ok, location: @employee }
        
      else
        @employees = Employee.where.not(:id => @employee.id)
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    @employee.destroy

    uri = URI.parse( "http://socialnetworkusers.azurewebsites.net/api/Users/#{@employee.social_network_user_id}" )
    request = Net::HTTP::Delete.new(uri.path)
    http = Net::HTTP.new(uri.host, uri.port)

    respond_to do |format|
      begin
        response = http.request(request)
      rescue
        format.html { redirect_to employees_url, notice: 'Employee was successfully deleted but social network could not be reached.' }
        format.json { head :no_content }
        return
      end

      if response.code == '200'
        format.html { redirect_to employees_url, notice: 'Employee was successfully deleted.' }
      else
        format.html { redirect_to employees_url, notice: 'Employee was successfully deleted but was not removed from the social network.' }
      end
      format.json { head :no_content }
     
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
      if @employee.social_network_user_id
        uri = URI.parse("http://socialnetworkusers.azurewebsites.net/api/Users/#{@employee.social_network_user_id}" )
        http = Net::HTTP.new(uri.host, uri.port)
        request = Net::HTTP::Get.new(uri.path)
        begin
          response = http.request(request)
          if response.code == '200'
            social_network_user = JSON.parse(response.body)
            @employee.set_attributes_from_social_media_service(social_network_user)
          else
            logger.error 'Social network service is not available.'
          end
        rescue
          logger.error 'Social network service is not available.'
        end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:firstname, :lastname, :pkz, :street, :street_nr, :post_code, :city, :country, :position, :room_number, :superior_id, :telephone, :email, :social_security_numer, :user_name, :credit_card_nr, :expiration_date_time)
    end

    def authenticate_user!
      unless session['google_auth_data']
        respond_to do |format|
          format.html { redirect_to get_back_url, :notice => 'Please sign in first!' }
          format.json { render json: {}, :status => 401 }
        end
      end
    end

    def get_back_url
      if request.env["HTTP_REFERER"].present? and request.env["HTTP_REFERER"] != request.env["REQUEST_URI"]
        :back
      else
        employees_path
      end
    end
end
