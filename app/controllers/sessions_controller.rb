class SessionsController < ApplicationController

  skip_before_action :verify_authenticity_token, :only => [:developer_callback]

  swagger_controller :sessions, "Session Management"

  swagger_api :sign_in do
    summary "Signs the user in"
    notes "Redirects the user to the identity swervice provider"
    response 302, "Redirection to the identity service provider"
  end

  swagger_api :sign_out do
    summary "Signs the user out"
    notes "Deletes the sessions cookie"
    response :ok
  end

  def sign_in
    unless Rails.env.production?
      redirect_to '/auth/developer'
    else
      redirect_to '/auth/google_oauth2'
    end
  end

  # GET    /auth/google_oauth2/callback
  # POST   /auth/google_oauth2/callback
  def google_oauth2_callback
    if request.env['omniauth.auth'].info.name.blank?
      redirect_to '/auth/google_oauth2'
      return
    end
    sign_in_user
  end

  # GET    /auth/developer/callback
  # POST   /auth/developer/callback
  def developer_callback
    unless Rails.env.production?
      if request.env['omniauth.auth'].info.name.blank?
        redirect_to '/auth/developer'
        return
      end
      sign_in_user
    else
      render json: {}, :status => 404
    end
  end

  # GET    /auth/failure
  def failure
    session['google_auth_data'] = nil
    respond_to do |format|
      format.html { redirect_to employees_path, :notice => "Unable to sign in." }
      format.json { render json: {}, :status => 401 }
    end
  end

  # DELETE /sessions/sign_out
  def sign_out
    name = session['google_auth_data']['name']
    session['google_auth_data'] = nil
    respond_to do |format|
      format.html { redirect_to employees_path, :notice => "Signed out successfully. Good bye #{name}." }
      format.json { render json: {}, :status => 204 }
    end
  end

  private

  def sign_in_user
    auth = request.env['omniauth.auth']
    session['google_auth_data'] = auth.info
    response.headers['X-CSRF-Token'] = form_authenticity_token
    respond_to do |format|
      format.html { redirect_to employees_path, :notice => "Signed in successfully. Welcome #{auth.info.name}." }
      format.json { render json: {}, :status => 204 }
    end
  end
end
