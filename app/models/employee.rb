class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      record.errors[attribute] << (options[:message] || "you typed in is not a correct email")
    end
  end
end

class Employee < ActiveRecord::Base

  attr_accessor :user_name, :credit_card_nr, :expiration_date

  validates :firstname, :lastname, :user_name, :expiration_date, presence: true, length: { maximum: 30 }
  validates :credit_card_nr, presence: true, length: { maximum: 30 }, numericality: { only_integer: true }
  validates :city, :street, :position, length: { maximum: 20 }
  validates :email, presence: true, email: true, length: { maximum: 30 }
  validates :post_code, :street_nr, presence: true, length: { maximum: 8 }, numericality: { only_integer: true }
  validates :pkz, length: { maximum: 4 }, numericality: { only_integer: true, allow_blank: true }
  validates :telephone, length: { minimum: 3, maximum: 25 }, numericality: { only_integer: true, allow_blank: true }
  validates :room_number, length: { maximum: 4 }, numericality: { only_integer: true, allow_blank: true }
  validates :social_security_numer, length: { maximum: 15 }, numericality: { only_integer: true, allow_blank: true }
  validate :expiration_date_cannot_be_in_the_past

  def expiration_date_time
    self.expiration_date
  end

  def expiration_date_time=(text)
    self.expiration_date = text.to_date
  end

  def set_attributes_from_social_media_service(json)
    self.social_network_user_id = json['Id']
    self.user_name = json['UserName']
    self.credit_card_nr = json['CreditCardNr']
    self.expiration_date = json['ExpirationDate'].to_date
  end

  private

  def expiration_date_cannot_be_in_the_past
    if self.expiration_date_time.present? && self.expiration_date_time < Date.today
      errors.add(:expiration_date, "can't be in the past")
    end
  end    
end
