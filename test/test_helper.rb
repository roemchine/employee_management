ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  def prepare_omniauth
    OmniAuth.config.test_mode = true
    OmniAuth.config.mock_auth[:google] = OmniAuth::AuthHash.new({
      :provider => 'google',
      :uid => '123545',
      :info => { :name => 'Romana' }
    })
    request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:google]
  end

  def sign_in_user
    prepare_omniauth
    session['google_auth_data'] = request.env["omniauth.auth"].info
  end
end
