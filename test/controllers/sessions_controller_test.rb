require 'test_helper'

class SessionsControllerTest < ActionController::TestCase

  test "should get sign_in" do
    get :sign_in
    assert_response :redirect
    assert_redirected_to '/auth/developer'
  end

  test "should get google_oauth2_callback" do
    prepare_omniauth
    get :google_oauth2_callback
    assert_response :redirect
    assert_redirected_to employees_path
    assert_not_nil flash[:notice]
  end

  test "should get developer_callback" do
    prepare_omniauth
    get :developer_callback
    assert_response :redirect
    assert_redirected_to employees_path
    assert_not_nil flash[:notice]
  end

  test "should get failure" do
    get :failure
    assert_response :redirect
    assert_redirected_to employees_path
    assert_not_nil flash[:notice]
  end

  test "should get sign_out" do
    sign_in_user
    get :sign_out
    assert_response :redirect
    assert_redirected_to employees_path
    assert_not_nil flash[:notice]
  end

end
