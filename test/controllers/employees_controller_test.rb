require 'test_helper'

class EmployeesControllerTest < ActionController::TestCase
  setup do
    @employee = employees(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:employees)
  end

  test "should get new" do
    sign_in_user
    get :new
    assert_response :success
  end

  test "should create employee" do
    sign_in_user
    assert_difference('Employee.count') do
      post :create, employee: { street: @employee.street, street_nr: @employee.street_nr, post_code: @employee.post_code, city: @employee.city, country: @employee.country, email: @employee.email, firstname: @employee.firstname, lastname: @employee.lastname, pkz: @employee.pkz, position: @employee.position, room_number: @employee.room_number, social_security_numer: @employee.social_security_numer, superior_id: @employee.superior_id, telephone: @employee.telephone, user_name: 'maxilein', credit_card_nr: 232323434645, expiration_date_time: "2033-02-06T22:15:38.977Z" }
    end

    assert_redirected_to employee_path(assigns(:employee))
  end

  test "should show employee" do
    get :show, id: @employee
    assert_response :success
  end

  test "should get edit" do
    sign_in_user
    get :edit, id: @employee
    assert_response :success
  end

  test "should update employee" do
    sign_in_user
    patch :update, id: @employee, employee: { street: @employee.street, street_nr: @employee.street_nr, post_code: @employee.post_code, city: @employee.city, country: @employee.country, email: @employee.email, firstname: @employee.firstname, lastname: @employee.lastname, pkz: @employee.pkz, position: @employee.position, room_number: @employee.room_number, social_security_numer: @employee.social_security_numer, superior_id: @employee.superior_id, telephone: @employee.telephone, user_name: 'hanni', credit_card_nr: 45456456456, expiration_date_time: "2033-02-06T22:15:38.977Z" }
    assert_redirected_to employee_path(assigns(:employee))
  end

  test "should destroy employee" do
    sign_in_user
    assert_difference('Employee.count', -1) do
      delete :destroy, id: @employee
    end

    assert_redirected_to employees_path
  end
end
